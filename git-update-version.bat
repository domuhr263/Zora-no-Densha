::=========================================================
:: Batch file which temporarily updates the PATH variable
::  and the calls for the git installation to consider and
::  execute the version update script at this location.
:: Resets any changes to PATH after execution.
::=========================================================
@echo off
setlocal
set "PATH=%PATH%;%~dp0"
git update-version
endlocal
